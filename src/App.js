import Cadastro from "./pages/Cadastro/Cadastro";
import Header from "./components/Header/Header";

function App() {
  return (
    <div className="App">
      <Header />
      <Cadastro />
    </div>
  );
}

export default App;
