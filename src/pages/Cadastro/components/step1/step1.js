import React from "react";
import { Container } from "./styles";
import { Form, Row, Col, Button, Input, Select, Typography } from "antd";
import FloatLabel from "../FloatLabel/FloatLabel";

import axios from "axios";

const { Option } = Select;
const { Text } = Typography;

export default function Step1({ data, setData }) {
  const onChangeNumero = (value) => {
    setData({
      ...data,
      numero: value,
    });
  };

  const onChangeNome = (value) => {
    setData({
      ...data,
      nome: value,
    });
  };

  const onChangeValidade = (value) => {
    setData({
      ...data,
      validade: value,
    });
  };

  const onChangeCVC = (value) => {
    setData({
      ...data,
      codigo: value,
    });
  };

  const validatorName = (rule, value, callback) => {
    try {
      if (value.indexOf(" ") > 0) {
        callback();
      } else {
        throw new Error("Insira seu nome completo");
      }
    } catch (err) {
      callback(err);
    }
  };

  const onFinish = (fieldsValue) => {
    const values = {
      numero: fieldsValue["numero"],
      nome: fieldsValue["nome"],
      validade: fieldsValue["validade"],
      cvv: fieldsValue["cvv"],
      parcelas: fieldsValue["parcelas"],
    };

    axios.post("/pagar", values).then(function (response) {
      console.log(response);
    });
  };

  return (
    <Container>
      <Form initialValues={{ remember: true }} onFinish={onFinish}>
        <Row>
          <Col span={24}>
            <FloatLabel
              label="Número do cartão"
              name="numeroFloat"
              value={data.numero}
            >
              <Form.Item
                name="numero"
                rules={[
                  { required: true, message: "Número de cartão inválido" },
                  {
                    pattern: /^(?:\d*)$/,
                    message: "Número de cartão inválido",
                  },
                ]}
              >
                <Input onChange={(e) => onChangeNumero(e.target.value)} />
              </Form.Item>
            </FloatLabel>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FloatLabel
              label="Nome (igual ao cartão)"
              name="nome"
              value={data.nome}
            >
              <Form.Item
                name="nome"
                rules={[
                  {
                    validator: validatorName,
                  },
                ]}
              >
                <Input
                  onChange={(e) => {
                    onChangeNome(e.target.value);
                  }}
                />
              </Form.Item>
            </FloatLabel>
          </Col>
        </Row>
        <Row>
          <Col span={11}>
            <FloatLabel label="Validade" name="validade" value={data.validade}>
              <Form.Item
                name="validade"
                rules={[{ required: true, message: "Data inválida" }]}
              >
                <Input onChange={(e) => onChangeValidade(e.target.value)} />
              </Form.Item>
            </FloatLabel>
          </Col>
          <Col span={11} offset={2}>
            <FloatLabel
              label="CVV"
              name="cvv"
              maxLength={3}
              value={data.codigo}
            >
              <Form.Item
                name="cvv"
                rules={[
                  { required: true, message: "Código inválido" },
                  {
                    pattern: /^(?:\d*)$/,
                    message: "O código CVV só aceita números",
                  },
                  {
                    pattern: /^[\d]{0,4}$/,
                    message: "O código CVV tem que ter menos de 4 caracteres",
                  },
                ]}
              >
                <Input
                  onChange={(e) => onChangeCVC(e.target.value)}
                  onFocus={() => setData({ ...data, focus: true })}
                  onBlur={() => setData({ ...data, focus: false })}
                />
              </Form.Item>
            </FloatLabel>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form.Item
              name="parcelas"
              rules={[
                { required: true, message: "Insira o número de parcelas" },
              ]}
            >
              <Select
                style={{ width: "100%" }}
                placeholder="Número de parcelas"
              >
                <Option value="1">1x - sem juros</Option>
                <Option value="2">2x - sem juros</Option>
                <Option value="3">3x - sem juros</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col xs={{ span: 24, offset: 0 }} lg={{ span: 12, offset: 12 }}>
            <Form.Item>
              <Button className="button-continue" htmlType="submit">
                <Text className="text-button-continue">CONTINUAR</Text>
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Container>
  );
}

// TO DO
// - Validacoes
// - Deixar Bonito :)
