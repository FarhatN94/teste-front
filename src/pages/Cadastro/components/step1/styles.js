import styled from "styled-components";

export const Container = styled.div`
  .button-continue {
    width: 100%;
    height: 51px;
    background-color: #de4b4b;
    border-radius: 10px;
  }

  .text-button-continue {
    text-align: center;
    font-size: 17px;
    letter-spacing: -0.01px;
    color: #ffffff;
    opacity: 1;
  }

  .ant-input {
    outline: 0;
    border-width: 0 0 1px;
    border-color: #c9c9c9;
    padding: 14px 11px 2px 11px !important;
  }

  .ant-input:hover {
    border-right-width: 0px !important;
  }

  .ant-input:focus,
  .ant-input-focused {
    border-color: #c9c9c9;
    border-right-width: 0px !important;
    outline: 0;
    box-shadow: 0 0 0 0px rgb(24 144 255 / 20%);
  }

  .ant-select-selector {
    border: 0px solid #d9d9d9 !important;
    border-bottom: 1px solid #de4b4b !important;
  }

  .ant-select-arrow {
    color: #de4b4b;
  }

  .ant-select-selection-placeholder {
    font: normal normal normal 15px/21px Verdana;
    letter-spacing: 0px;
    color: #c9c9c9;
    opacity: 1;
  }

  .col-icon-add {
    display: flex;
    align-self: center;
  }
`;
