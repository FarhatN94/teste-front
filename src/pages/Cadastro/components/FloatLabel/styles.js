import styled from "styled-components";

export const Container = styled.div`
  width: 100% !important;
  .float-label {
    position: relative;
    margin-bottom: 17px;
    width: 100% !important;
  }

  .label {
    font-size: 17px;
    font-weight: normal;
    position: absolute;
    pointer-events: none;
    left: 12px;
    top: 12px;
    transition: 0.2s ease all;
    color: #c9c9c9;
  }

  .label-float {
    top: 6px;
    font-size: 10px;
  }
`;
