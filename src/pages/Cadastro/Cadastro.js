import React, { useState } from "react";
import { Row, Col, Typography, Steps } from "antd";
import Icon from "@ant-design/icons";
import Cards from "react-credit-cards";
import { Container } from "./styles";

import Step1 from "./components/step1/step1";

import { ReactComponent as IconeCredito } from "../../assets/credit-icon.svg";
import { LeftOutlined } from "@ant-design/icons";

const { Text } = Typography;
const { Step } = Steps;

export default function Cadastro() {
  const [steps] = useState(0);
  const [data, setData] = useState({
    numero: null,
    nome: null,
    validade: null,
    codigo: null,
    focus: false,
    parcelas: null,
  });

  return (
    <Container>
      <Row>
        <Col xs={24} lg={6}>
          <Row className="row-change-payment">
            <Col>
              <LeftOutlined style={{ color: "#ffffff" }} />
            </Col>
            <Col>
              <Text className="change-payment-text">
                Alterar forma de pagamento
              </Text>
            </Col>
            <Col span={22} className="col-steps-mobile">
              <Text className="text-steps-mobile">Etapa 2 de 3</Text>
            </Col>
          </Row>
          <Row className="row-title-add">
            <Col
              xs={{ span: 2, offset: 0 }}
              md={{ span: 2, offset: 0 }}
              lg={{ span: 4, offset: 0 }}
              xl={{ span: 4, offset: 0 }}
              className="col-icon-add"
            >
              <Icon component={IconeCredito} style={{ fontSize: "50px" }} />
            </Col>
            <Col
              xs={{ span: 12, offset: 3 }}
              sm={{ span: 8, offset: 1 }}
              md={{ span: 4, offset: 0 }}
              lg={{ span: 18, offset: 2 }}
              xl={{ span: 18, offset: 1 }}
            >
              <Text className="text-add-card">
                Adicione um novo cartão de crédito
              </Text>
            </Col>
          </Row>
          <Row className="row-card">
            <Col>
              <Cards
                cvc={data.codigo ? data.codigo : ""}
                expiry={data.validade ? data.validade : ""}
                focused={data.focus ? "cvc" : ""}
                name={data.nome ? data.nome : ""}
                number={data.numero ? data.numero : ""}
              />
            </Col>
          </Row>
        </Col>
        <Col xs={24} lg={18}>
          <Row className="row-steps">
            <Col span={16}>
              <Steps size="small" current={1}>
                <Step title="Carrinho" />
                <Step title="Pagamento" />
                <Step title="Confirmação" />
              </Steps>
            </Col>
          </Row>
          <Row className="row-form">
            <Col xs={24} lg={16}>
              {steps === 0 && <Step1 data={data} setData={setData} />}
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}
