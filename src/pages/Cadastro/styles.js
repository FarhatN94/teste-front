import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 90vh;
  background: linear-gradient(90deg, #de4b4b 25%, #ffffff 0%);

  .text-add-card {
    font-size: 20px;
    font-weight: bold;
    color: #ffffff;
  }

  .rccs__card {
    height: 224px;
    width: 364px;
  }

  .change-payment-text {
    font-size: 13px;
    color: #ffffff;
    padding: 0 0 0 14px;
  }

  .ant-steps-item-finish .ant-steps-item-icon {
    background-color: #de4b4b;
    border-color: #fff;
  }

  .ant-steps-item-finish .ant-steps-item-icon > .ant-steps-icon {
    color: white;
  }

  .ant-steps-item-finish
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-title {
    color: #de4b4b;
    font-weight: bold;
  }

  .ant-steps-item-finish
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-title::after {
    background-color: #de4b4b;
  }

  .ant-steps-item-process .ant-steps-item-icon {
    background-color: #fff;
    border-color: #de4b4b;
  }

  .ant-steps-item-process
    > .ant-steps-item-container
    > .ant-steps-item-icon
    .ant-steps-icon {
    color: #de4b4b;
  }

  .ant-steps-item-process
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-title {
    color: #de4b4b;
  }

  .row-change-payment {
    display: flex;
    justify-content: left;
    padding: 53px 0px 54px 68px;
  }

  .row-title-add {
    padding: 0 0px 31px 64px;
  }

  .row-card {
    padding: 0 0 0 64px;
  }

  .row-steps {
    padding: 50px 0 50px 175px;
  }

  .row-form {
    padding: 0px 0px 67px 168px;
  }

  .col-steps-mobile {
    display: none;
  }

  @media (max-width: 992px) {
    background: linear-gradient(180deg, #de4b4b 30%, #ffffff 0%);
    .change-payment-text {
      display: none;
    }

    .row-steps {
      display: none;
    }

    .col-steps-mobile {
      display: flex;
      justify-content: center;
    }

    .row-card {
      padding: 0;
      justify-content: center;
    }

    .row-title-add {
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .row-form {
      padding: 0;
      width: 100%;
    }

    .rccs__card {
      width: 280px;
      height: 172px;
    }
    .row-change-payment {
      padding: 13px 0px 20px 24px !important;
    }
    .text-add-card {
      font-size: 16px;
    }

    .text-steps-mobile {
      color: #ffffff;
    }

    .ant-form {
      padding: 0 20px 0 20px;
    }
  }

  @media (max-width: 1140px) {
    .row-change-payment {
      padding: 53px 0px 54px 24px;
    }

    .row-title-add {
      padding: 0 0px 31px 24px;
    }

    .row-card {
      padding: 0 0 0 24px;
    }
  }
`;
