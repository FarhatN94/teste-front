import React from "react";
import { Container } from "./styles";
import { Row, Col, Button } from "antd";
import { ReactComponent as Logo } from "../../assets/logo.svg";

export default function Header() {
  return (
    <Container>
      <nav>
        <Row className="row-header">
          <Col className="col-header" span={8}>
            <Logo />
          </Col>
          <Col className="col-header" span={2} offset={3}>
            <Button className="button-header">a</Button>
          </Col>
          <Col className="col-header" span={2}>
            <Button className="button-header">a</Button>
          </Col>
          <Col className="col-header" span={2}>
            <Button className="button-header">a</Button>
          </Col>
          <Col className="col-header" span={2}>
            <Button className="button-header">a</Button>
          </Col>
          <Col className="col-header" span={2}>
            <Button className="button-header">a</Button>
          </Col>
        </Row>
      </nav>
    </Container>
  );
}
