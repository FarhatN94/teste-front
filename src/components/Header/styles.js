import styled from "styled-components";

export const Container = styled.div`
  nav {
    height: 70px;
    border: 1px solid #dfdfdf;
  }

  .button-header {
    background-color: #3c3c3c;
    width: 100px;
    height: 18px;
    border-radius: 5px;
    font-size: 6px;
    color: #3c3c3c;
  }

  .row-header {
    height: 100%;
  }

  .col-header {
    height: 100%;
    justify-content: center;
    align-items: center;
    display: flex;
  }

  @media (max-width: 992px) {
    nav {
      display: none;
    }
  }
`;
